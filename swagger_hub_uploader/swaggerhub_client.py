# API key: 78f4b029-3f46-4528-b6d8-2f20d08c9413

import requests
import os
import sys

URL = "https://api.swaggerhub.com/apis/"
OWNER = os.environ['SWAGGERHUB_OWNER']
querystring = {"isPrivate": "true", "version": "1", "oas": "2.0", "force": "true"}  # Params del metodo post

API_KEY = os.environ['SWAGGERHUB_API_KEY']

headers = {
    'Content-Type': "application/json",
    'Authorization': API_KEY,
    'User-Agent': "PostmanRuntime/7.15.2",
    'Accept': "*/*",
    'Cache-Control': "no-cache",
    'Postman-Token': "39f86058-4389-455e-992d-7b177d7723aa,aded704b-558b-4738-a101-67c5367574cd",
    'Host': "api.swaggerhub.com",
    'Accept-Encoding': "gzip, deflate",
    'Content-Length': "25993",
    'Connection': "keep-alive",
    'cache-control': "no-cache"
}


def build_url(name):
    return URL + OWNER + "/" + name


def process_swagger_descriptor(swagger_folder):
    server_path = os.path.join(swagger_folder, "swagger.json")
    return open(server_path, 'r').read()


def create_swaggerhub_documentation(name, swagger_folder):
    return requests.request(
        "POST",
        build_url(name),
        data=process_swagger_descriptor(swagger_folder),
        headers=headers,
        params=querystring)


if __name__ == "__main__":
    create_swaggerhub_documentation(sys.argv[1], sys.argv[2])
