import unittest

import os

import swagger_hub_uploader.swaggerhub_client as target


class TestSwaggerhubClient(unittest.TestCase):

    def test_build_url(self):
        input_name = "ositopoo666_api"
        expected_value = "https://api.swaggerhub.com/apis/josefz/ositopoo666_api"
        actual_value = target.build_url(input_name)
        self.assertEqual(expected_value, actual_value)

    def test_create_swaggerhub_documentation(self):
        api_name = 'PETSTORE_INTERCORP4'
        swagger_folder = os.getcwd()
        response = target.create_swaggerhub_documentation(api_name, swagger_folder).ok
        self.assertTrue(response)


if __name__ == "__main__":
    unittest.main()
